User.destroy_all
User.create(
  nickname: 'admin',
  email: 'admin@admin.com',
  password: 'qweasdzxc',
  password_confirmation: 'qweasdzxc'
)

Category.destroy_all
category = Category.create({ name: 'One' })
category1 = Category.create({ name: 'Two' })
category2 = Category.create(  { name: 'Three' })

Product.destroy_all
Product.create([
  { title: 'tie', price: 1200, category: category },
  { title: 'glasses', price: 750, category: category },
  { title: 'belt ', price: 1000, category: category1 } ,
  { title: 'wristwatch', price: 1500, category: category1  },
  { title: 'baseball cap', price: 500, category: category2 }
])

