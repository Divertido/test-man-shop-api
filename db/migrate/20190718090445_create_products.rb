class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :title
      t.references :user, foreign_key: true
      t.decimal :price, precision: 8, scale: 2, default: 0.0

      t.timestamps
    end
  end
end
