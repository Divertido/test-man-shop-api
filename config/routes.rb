Rails.application.routes.draw do
  post 'refresh', to: 'refresh_session#create'
  post 'signup', to: 'registration#create'
  post 'signin', to: 'session#create'
  delete 'signin', to: 'session#destroy'

  root 'products#index'

  get 'carts/:id', to: "carts#show"
  delete 'carts/:id', to: "carts#destroy"

  post 'line_items/:id/add' => "line_items#add_quantity", as: "line_item_add"
  post 'line_items/:id/reduce' => "line_items#reduce_quantity", as: "line_item_reduce"
  post 'line_items' => "line_items#create"
  get 'line_items/:id' => "line_items#show", as: "line_item"
  delete 'line_items/:id' => "line_items#destroy"
  
  resources :products
  resources :orders
end
